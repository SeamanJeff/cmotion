/*

 cmotion.js - Runs under node.js, analyzes the cache motion of a (djb) dnscache
 server running as a Docker comtainer, and calculates the cache cycle time.

 See https://cr.yp.to/djbdns/cachesize.html.

 This is literally my second es6 program ever, so have mercy.

 */
"use strict";

const Docker = require('node-docker-api').Docker;
const moment = require('moment');
const _ = require('lodash');

//we will find the current cache size and store it here
let cacheSize;

//a list of stats data we will store
let statList = [];

let docker = new Docker({socketPath: '/var/run/docker.sock'});

//Configuration: 
const containerName = 'jad_dnscache_1';
const logLines = 100000;

//extract the value of the CACHESIZE environment variable from the Docker container
//that is running dnscache
docker.container.status(containerName)
	.then((cStatus) => {
		//find the environment variable CACHESIZE
		let line = _.find(cStatus.Config.Env, function (o) {
			return o.match('CACHESIZE.*')
		});
		cacheSize = line.split('=')[1];
	})

	//collect a bunch of dnscache logs and pick out the "stats" entries and accumulate them
	.then(() => {
		docker.container.logs({stderr: 1, stdout: 1, timestamps: 1, tail: logLines}, containerName)
			.then((stream) => {
				stream.on('data', (info) => {
					//first 8 bytes is a header indicating which stream and how many bytes
					let ary = info.toString('utf-8').slice(8).split(' ');
					if (ary[1] === 'stats') {
						statList.push({
							time: moment(ary[0]),
							queryCount: parseInt(ary[2], 10),
							cacheMotion: parseInt(ary[3], 10)
						})
					}
				});
				stream.on('end', () => {
					console.log('Current Cache Size: ' + cacheSize);
					console.log('Summarizing ' + logLines + " log lines with " + statList.length + " stats entries");
					//calculate the time interval between our first and last data point
					let minutes = (statList[statList.length - 1].time.diff(statList[0].time, 'minutes'));
					console.log('Minutes: ' + minutes);
					const dayInMinutes = 24 * 60;
					// the fraction of a day for which we are measuring the cache motion
					let sampleFraction = parseInt(dayInMinutes / minutes, 10);
					console.log('Fraction of day: ' + sampleFraction);
					// the change in the cachemotion stat during the sample interval
					let motion = (statList[statList.length - 1].cacheMotion - statList[0].cacheMotion);
					console.log('Raw motion: ' + motion);
					// extrapolate the motion for a full day
					let extrapolatedMotion = motion * sampleFraction;
					console.log('Extrapolated daily motion: ' + extrapolatedMotion);
					// after cycleTime days, the entire cache will have been overwritten
					let cycleTime = Math.round(cacheSize / extrapolatedMotion * 100) / 100;
					console.log("Cycle Time: " + cycleTime + " days");
				});
				stream.on('error', (err) => console.log(err)
				)
			})
			.catch((error) => console.log(error)
			)
	});
