# cmotion.js #

Runs under node.js, analyzes the cache motion of a (djb) dnscache
server running as a Docker comtainer, and calculates the cache cycle time.

See [https://cr.yp.to/djbdns/cachesize.html](https://cr.yp.to/djbdns/cachesize.html).

### Why would you need this? ###

* If you are running a dnscache (djbdns) under Docker ([like this](https://bitbucket.org/SeamanJeff/jadns))
* If you want to gauge the effectiveness of the cache size you have chosen

### A note on the impact of CDNs ###

If you analyze what names are being looked up you may be shocked at what proportion belong to CDNs ([Content Delivery Networks](https://en.wikipedia.org/wiki/Content_delivery_network)).


### Configuration ###

The following lines in the script determine which container the script extracts log lines from and how many log lines are analyzed:

~~~~
//Configuration:
const containerName = 'jad_dnscache_1';
const logLines = 100000;
~~~~

### Example Run ###

~~~~
$ node cmotion.js
Current Cache Size: 10000000
Summarizing 100000 log lines with 3287 stats entries
Minutes: 102
Fraction of day: 14
Raw motion: 745591
Extrapolated daily motion: 10438274
Cycle Time: 0.96 days
~~~~

### Interpreting Results ###

See [DJB's original page on the subject](https://cr.yp.to/djbdns/cachesize.html).